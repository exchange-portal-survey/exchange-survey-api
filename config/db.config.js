const mongoose = require('mongoose');
const log = require('simple-node-logger').createSimpleLogger();

const uri = process.env.MONGO_DB_URI || 'mongodb://localhost:27017/test'
let _db;

async function connect() {
  try {
    _db = await mongoose.connect(uri);
    log.info('Connected to MongoDB');
  } catch (err) {
    console.error(err);
  }
}

module.exports = { connect }
