const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');

const log = require('simple-node-logger').createSimpleLogger();

const app = express();

const { connect } = require('../config/db.config');
const survey = require('./routes/survey');
const password = require('./routes/password');
const averages = require('./routes/responses/averages');
const allResponses = require('./routes/responses/allResponses');
const responseCounts = require('./routes/responses/responseCounts');

const port = 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

app.use('/survey', survey);
app.use('/survey/password', password);
app.use('/survey/averages', averages);
app.use('/survey/responses', allResponses);
app.use('/survey/responseCount', responseCounts);

app.listen(port, async () => {
  await connect();
  log.info(`App listening on port ${port}`);
});

module.exports = app;
