const { Response } = require('../models/response.model');
const { RESPONSE_COUNTS_ALL_PIPELINE, getResponseCountsPipelineByDate } = require('../pipelines/responseCounts');

function createResponseCountsObjects(responseCounts) {
  const questions = responseCounts.map(count => count._id);
  const uniqueQuestions = [...new Set(questions)];
  const responseCountsObjects = [];
  uniqueQuestions.forEach((question) => {
    const responseArr = []
    responseCounts.forEach(response => {
      const responseQuestion = response._id;
      if (question === responseQuestion) {
        responseArr.push(response.counts);
      }
    })
    const questionObject = {
      question,
      counts: responseArr
    }
    responseCountsObjects.push(questionObject);
  });
  return responseCountsObjects;
}

async function getResponseCounts() {
  const responseCounts = await Response.aggregate(RESPONSE_COUNTS_ALL_PIPELINE);
  return createResponseCountsObjects(responseCounts);
}

async function getResponseCountsByDate(startDate, endDate) {
  const responseCounts = await Response.aggregate(getResponseCountsPipelineByDate(startDate, endDate));
  return createResponseCountsObjects(responseCounts);
}

module.exports = {
  getResponseCounts,
  getResponseCountsByDate
};
