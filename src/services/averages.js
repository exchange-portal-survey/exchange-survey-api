const { Response } = require('../models/response.model');
const { AVERAGES_ALL_PIPELINE, getAveragesByDatePipeline } = require('../pipelines/averages');

async function getResponseAverages() {
  return Response.aggregate(AVERAGES_ALL_PIPELINE);
}

async function getResponseAveragesBetweenDates(startDate, endDate) {
  return Response.aggregate(getAveragesByDatePipeline(startDate, endDate));
}

module.exports = {
  getResponseAverages,
  getResponseAveragesBetweenDates,
};
