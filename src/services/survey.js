const { Response } = require('../models/response.model');

async function getSurveyResponses() {
  const surveyResponses = await Response.find({});
  const surveyResponseArray = [];
  [...surveyResponses].forEach((surveyResponse) => {
    surveyResponseArray.push(surveyResponse);
  });
  return surveyResponseArray;
}

async function getSurveyResponsesBetweenDates(startDate, endDate) {
  const surveyResponses = await Response.find({
    "createdAt": {
      $gte: new Date(startDate),
      $lte: new Date(`${endDate}T23:59:59`)
    }
  });
  const surveyResponseArray = [];
  [...surveyResponses].forEach((surveyResponse) => {
    surveyResponseArray.push(surveyResponse);
  });
  return surveyResponseArray;
}

module.exports = {
  getSurveyResponses,
  getSurveyResponsesBetweenDates
}
