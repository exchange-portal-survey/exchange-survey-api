const express = require('express');
const log = require('simple-node-logger').createSimpleLogger();
const { Response } = require('../models/response.model');
const { getSurveyResponses, getSurveyResponsesBetweenDates } = require('../services/survey');

const router = express.Router();

router.get('/all', async (req, res) => {
  try {
    const hasQuery = Object.keys(req.query).length > 0
    if (hasQuery) {
      res.send(await getSurveyResponsesBetweenDates(req.query.start, req.query.end));
    } else {
      res.send(await getSurveyResponses());
    }
  } catch (error) {
    log.error(error);
  }
});

router.post('/', async (req, res) => {
  const response = new Response(req.body);
  try {
    await response.save();
    log.info('Response recorded successfully');
    res.send('Response recorded successfully');
  } catch (error) {
    log.error(error);
  }
});

module.exports = router;
