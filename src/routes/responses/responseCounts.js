const express = require('express');
const { getResponseCounts, getResponseCountsByDate } = require('../../services/responseCounts');
const log = require('simple-node-logger').createSimpleLogger();

const router = express.Router();
router.get('/', async (req, res) => {
  try {
    const hasQuery = Object.keys(req.query).length > 0
    if (hasQuery) {
      res.send(await getResponseCountsByDate(req.query.start, req.query.end));
    } else {
      res.send(await getResponseCounts());
    }
  } catch (error) {
    log.error(error);
  }
});

module.exports = router;
