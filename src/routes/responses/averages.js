const { getResponseAverages, getResponseAveragesBetweenDates } = require('../../services/averages');
const express = require('express');
const log = require('simple-node-logger').createSimpleLogger();

const router = express.Router();
router.get('/', async (req, res) => {
  try {
    const hasQuery = Object.keys(req.query).length > 0
    if (hasQuery) {
      res.send(await getResponseAveragesBetweenDates(req.query.start, req.query.end));
    } else {
      res.send(await getResponseAverages());
    }
  } catch (error) {
    log.error(error);
  }
});

module.exports = router;
