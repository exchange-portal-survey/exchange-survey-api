const express = require('express');
const log = require('simple-node-logger').createSimpleLogger();

const { Response } = require('../../models/response.model');

const router = express.Router();
router.get('/count', async (req, res) => {
  try {
    const numberOfResponses = await Response.find().countDocuments();
    res.send({ numberOfResponses });
  } catch (error) {
    log.error(error);
  }
});

module.exports = router;
