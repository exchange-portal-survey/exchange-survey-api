const bcrypt = require('bcryptjs');
const express = require('express');
const log = require('simple-node-logger').createSimpleLogger();

const { Password } = require('../models/password.model');

const router = express.Router();

router.post('/new', async (req, res) => {
  const newPassword = req.body.password;
  const saltRounds = 10;
  try {
    bcrypt.hash(newPassword, saltRounds, async (err, hash) => {
      const password = new Password({
        password: hash,
      });
      await password.save();
      log.info('Password added');
      res.send('Password added');
    });
  } catch (error) {
    log.error(error);
  }
});

router.post('/verify', async (req, res) => {
  const userPassword = req.body.password;
  try {
    Password.find({}, (err, allPasswordHashes) => {
      res.send(allPasswordHashes.some((passwordHash) => {
        const passwordHashString = passwordHash.password;
        // TODO: use async call for this
        return bcrypt.compareSync(userPassword, passwordHashString);
      }));
    });
  } catch (error) {
    log.error(error);
  }
});

module.exports = router;
