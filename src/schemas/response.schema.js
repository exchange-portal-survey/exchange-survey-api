const mongoose = require('mongoose');

const responseSchema = new mongoose.Schema({
  contact: {
    email: String,
    role: String,
  },
  responses: [
    {
      question: String,
      response: Number,
      _id: false,
    },
  ],
  feedback: String,
}, { timestamps: true });

module.exports = { responseSchema };
