const mongoose = require('mongoose');
const { responseSchema } = require('../schemas/response.schema');

const Response = mongoose.model('Response', responseSchema);

module.exports = { Response };
