const mongoose = require('mongoose');
const { passwordSchema } = require('../schemas/password.schema');

const Password = mongoose.model('Password', passwordSchema);

module.exports = { Password };
