const AVERAGES_ALL_PIPELINE = [
  {
    $unwind: '$responses',
  },
  {
    $group: {
      _id: '$responses.question',
      average: { $avg: '$responses.response' },
    },
  },
];

function getAveragesByDatePipeline(startDate, endDate) {
  return [
    {
      $unwind: '$responses',
    },
    {
      $match: {
        "createdAt": {
          $gte: new Date(startDate),
          $lte: new Date(`${endDate}T23:59:59`)
        }
      }
    },
    {
      $group: {
        _id: '$responses.question',
        average: { $avg: '$responses.response' },
      },
    },
  ]
}

module.exports = {
  AVERAGES_ALL_PIPELINE,
  getAveragesByDatePipeline
}
