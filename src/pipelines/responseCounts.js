const RESPONSE_COUNTS_ALL_PIPELINE = [
  {
    $project: {_id: 0, responses: 1},
  },
  {
    $unwind: '$responses',
  },
  {
    $group: {
      _id:
        {
          question: '$responses.question',
          response: '$responses.response',
        },
      count: {
        $sum: 1
      },
    },
  },
  {
    $project: {
      _id: '$_id.question',
      counts: {
        response: '$_id.response',
        count: '$count'
      }
    }
  }
];

function getResponseCountsPipelineByDate(startDate, endDate) {
  return [
    {
      $match: {
        "createdAt": {
          $gte: new Date(startDate),
          $lte: new Date(`${endDate}T23:59:59`)
        }
      }
    },
    {
      $project: {_id: 0, responses: 1},
    },
    {
      $unwind: '$responses',
    },
    {
      $group: {
        _id:
          {
            question: '$responses.question',
            response: '$responses.response',
          },
        count: {
          $sum: 1
        },
      },
    },
    {
      $project: {
        _id: '$_id.question',
        counts: {
          response: '$_id.response',
          count: '$count'
        }
      }
    }
  ];
}

module.exports = {
  RESPONSE_COUNTS_ALL_PIPELINE,
  getResponseCountsPipelineByDate
}
