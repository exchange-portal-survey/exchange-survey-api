const expect = require('chai').expect;
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../../src/server');

const { clearResponseCollection, generateResponse } = require('../../setup/initialise.test');

chai.use(chaiHttp);

describe('Returning all average response values', () => {
  beforeEach(async () => {
    await clearResponseCollection();
    await generateResponse('test@email.com', 'product-owner', 5, 3, 4, 1, 'test-feedback')
    await generateResponse('test@email.com', 'product-owner', 1, 2, 3, 5, 'test-feedback')
    await generateResponse('test@email.com', 'product-owner', 3, 1, 5, 3, 'test-feedback')
    await generateResponse('test@email.com', 'product-owner', 4, 5, 1, 2, 'test-feedback')
    await generateResponse('test@email.com', 'product-owner', 2, 4, 2, 2, 'test-feedback')
  });

  it('should return an object with the correct averages', async () => {
    const expectedAverages = [3, 3, 3, 2.6].sort();
    const response = await chai.request(server).get('/survey/averages');
    const responseBody = response.body;
    const actualAverages = responseBody.map((val) => val.average).sort();
    expect(response).to.have.status(200);
    expect(response).to.be.json;
    expect(actualAverages.every((val, index) => val === expectedAverages[index])).to.be.true;
  });

  it('should return the averages with the correct question', async () => {
    const expectedResponseBody = [
      {
        _id: "How would you rate the experience of locating assets on the Portal?",
        average: 3
      },
      {
        _id: "How would you rate the experience of completing the consume form?",
        average: 3
      },
      {
        _id: "How would you rate the quality of the Portal content?",
        average: 3
      },
      {
        _id: "How would you rate the quality of the Business and Technical documentation on the Portal?",
        average: 2.6
      }
    ];
    const response = await chai.request(server).get('/survey/averages');
    const responseBody = response.body;
    responseBody.forEach(actualResponse => {
      const expectedResponse = expectedResponseBody.find((res) => res._id === actualResponse._id);
      expect(expectedResponse.average).to.equal(actualResponse.average);
    })
  });
});

describe('Returning average response values between dates', () => {
  beforeEach(async () => {
    await clearResponseCollection();
    await generateResponse('test@email.com', 'product-owner', 5, 3, 1, 1, 'test-feedback', new Date('2022-10-10T09:00:00'));
    await generateResponse('test@email.com', 'product-owner', 5, 5, 2, 1, 'test-feedback', new Date('2022-10-10T09:00:00'));
    await generateResponse('test@email.com', 'product-owner', 4, 5, 1, 3, 'test-feedback', new Date('2022-10-14T09:00:00'));
    await generateResponse('test@email.com', 'product-owner', 3, 3, 1, 2, 'test-feedback', new Date('2022-10-15T09:00:00'));
    await generateResponse('test@email.com', 'product-owner', 2, 4, 1, 1, 'test-feedback', new Date('2022-10-16T09:00:00'));
  });

  it('should return an object with the correct averages', async () => {
    const startDate = '2022-10-14';
    const endDate = '2022-10-16';
    const expectedAverages = [3, 4, 1, 2].sort();
    const response = await chai.request(server).get(`/survey/averages?start=${startDate}&end=${endDate}`);
    const responseBody = response.body;
    const actualAverages = responseBody.map((val) => val.average).sort();
    expect(response).to.have.status(200);
    expect(response).to.be.json;
    expect(actualAverages.every((val, index) => val === expectedAverages[index])).to.be.true;
  });
});
