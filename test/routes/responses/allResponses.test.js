const expect = require('chai').expect;
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../../src/server');

const { clearResponseCollection, generateResponse } = require('../../setup/initialise.test');

chai.use(chaiHttp);

describe('Getting number of responses in database', () => {
  beforeEach(() => {
    clearResponseCollection();
  });

  it('should return 5 when there are 5 responses in database', async () => {
    await generateResponse('test@email.com', 'product-owner', 5, 3, 4, 1, 'test-feedback')
    await generateResponse('test@email.com', 'product-owner', 5, 3, 4, 1, 'test-feedback')
    await generateResponse('test@email.com', 'product-owner', 5, 3, 4, 1, 'test-feedback')
    await generateResponse('test@email.com', 'product-owner', 5, 3, 4, 1, 'test-feedback')
    await generateResponse('test@email.com', 'product-owner', 5, 3, 4, 1, 'test-feedback')
    const response = await chai.request(server).get('/survey/responses/count');
    expect(response).to.have.status(200);
    expect(response).to.be.json;
    expect(response.body.numberOfResponses).to.equal(5);
  });
});
