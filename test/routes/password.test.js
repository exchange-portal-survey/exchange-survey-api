const expect = require('chai').expect;
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../src/server');

const { Password } = require('../../src/models/password.model');
const { clearPasswordCollection } = require('../setup/initialise.test');
const bcrypt = require('bcryptjs');

chai.use(chaiHttp);

describe('Posting a new password', () => {
  const requestBody = {
    password: 'test-password'
  }

  beforeEach(async () => {
    await clearPasswordCollection();
  });

  it('should add a new password hash to the database', async () => {
    const response = await chai.request(server).post('/survey/password/new').send(requestBody);
    const numberOfEntries = await Password.collection.countDocuments();
    expect(response.text).to.equal('Password added');
    expect(numberOfEntries).to.equal(1);
  });

  it('should add a password hash which matches the password in the request', async () => {
    const response = await chai.request(server).post('/survey/password/new').send(requestBody);
    const passwordsObject = await Password.find();
    const password = passwordsObject[0].password;
    expect(response).to.have.status(200);
    expect(bcrypt.compareSync('test-password', password)).to.be.true;
  })
});

describe('Verifying password sent in request is present in database', () => {
  const requestBody = {
    password: 'test-password'
  }

  it('should respond true when the password is present', async () => {
    await chai.request(server).post('/survey/password/new').send(requestBody);
    const response = await chai.request(server).post('/survey/password/verify').send(requestBody);
    expect(response).to.have.status(200);
    expect(response.text).to.equal('true');
  });

  it('should respond false when the password is not present', async () => {
    const incorrectPasswordBody = {
      password: 'incorrect-password'
    };
    await chai.request(server).post('/survey/password/new').send(requestBody);
    const response = await chai.request(server).post('/survey/password/verify').send(incorrectPasswordBody);
    expect(response).to.have.status(200);
    expect(response.text).to.equal('false');
  });
})
