const expect = require('chai').expect;
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../src/server');

const { Response } = require('../../src/models/response.model');
const { clearResponseCollection, generateResponse } = require(
  '../setup/initialise.test');

chai.use(chaiHttp);

describe('Posting a new response', () => {
  beforeEach(() => {
    clearResponseCollection();
  });

  it('should post a new response', async () => {
    const requestBody = {
      "contact": {
        "email": "example@test.com",
        "role": "product-owner"
      },
      "responses": [
        {
          "question": "How would you rate the experience of locating assets on the Portal?",
          "response": "5"
        },
        {
          "question": "How would you rate the experience of completing the consume form?",
          "response": "2"
        },
        {
          "question": "How would you rate the quality of the Portal content?",
          "response": "1"
        },
        {
          "question": "How would you rate the quality of the Business and Technical documentation on the Portal?",
          "response": "4"
        }
      ],
      "feedback": "This is some test feedback from Postman"
    }
    const response = await chai.request(server).post('/survey').send(requestBody);
    expect(response).to.have.status(200);
    expect(response.text).to.equal('Response recorded successfully');
    const numberOfDocuments = await Response.collection.countDocuments();
    expect(numberOfDocuments).to.equal(1);
  });
});

describe('Getting responses', () => {
  beforeEach(async () => {
    await clearResponseCollection();
  });

  it('should return all responses', async () => {
    await generateResponse('test@email.com', 'product-owner', 5, 3, 4, 1, 'test-feedback')
    const response = await chai.request(server).get('/survey/all');
    const responseBody = response.body[0];
    expect(response).to.have.status(200);
    expect(responseBody.contact.email).to.equal('test@email.com');
    expect(responseBody.contact.role).to.equal('product-owner');
    expect(responseBody.responses[0].response).to.equal(5);
    expect(responseBody.responses[1].response).to.equal(3);
    expect(responseBody.responses[2].response).to.equal(4);
    expect(responseBody.responses[3].response).to.equal(1);
    expect(responseBody.feedback).to.equal('test-feedback');
  });

  it('should return all responses between two dates', async () => {
    const startDate = '2022-10-10';
    const endDate = '2022-10-11';
    await generateResponse('test@email.com', 'product-owner', 5, 3, 4, 1, 'test-feedback', new Date('2022-10-10T09:00:00'));
    await generateResponse('test@email.com', 'product-owner', 5, 3, 4, 1, 'test-feedback', new Date('2022-10-11T09:00:00'));
    const response = await chai.request(server).get(`/survey/all?start=${startDate}&end=${endDate}`);
    const responseBody = response.body;
    const expectedResponseCount = 2;
    expect(responseBody.length).to.equal(expectedResponseCount);
  })
});
