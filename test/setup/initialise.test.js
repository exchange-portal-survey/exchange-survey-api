const { Response } = require('../../src/models/response.model');
const { Password } = require('../../src/models/password.model');

async function clearResponseCollection() {
  await Response.collection.drop();
}

async function clearPasswordCollection() {
  await Password.collection.drop();
}

async function generateResponse(email, role, responseOne, responseTwo, responseThree, responseFour,
                                feedback, createdAt) {
  const response = new Response({
    "contact": {
      "email": email,
      "role": role
    },
    "responses": [
      {
        "question": "How would you rate the experience of locating assets on the Portal?",
        "response": responseOne
      },
      {
        "question": "How would you rate the experience of completing the consume form?",
        "response": responseTwo
      },
      {
        "question": "How would you rate the quality of the Portal content?",
        "response": responseThree
      },
      {
        "question": "How would you rate the quality of the Business and Technical documentation on the Portal?",
        "response": responseFour
      }
    ],
    "feedback": feedback,
    "createdAt": createdAt ?? new Date()
  });
  await response.save();
}

module.exports = {
  clearResponseCollection,
  clearPasswordCollection,
  generateResponse
}
