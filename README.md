# Exchange Survey API

This project is the backend API to enable the Exchange Survey Frontend to interact with the MongoDB
database to store survey responses and for user authentication using password checks.

## Running the service

When running, the API will be available at `http://localhost:3000`.

### Prerequisites

This service connects to a MongoDB cluster on start-up, therefore it will not start up if there is
no MongoDB cluster is running. The safest and easiest way to run this service as part of the main
application is using the `docker-compose` command in the `exchange-survey-frontend` project, or
by using the Docker commands listed below.

### Docker

Run the following commands to start the service using Docker.

```bash
docker build . -t exchange-survey-api
docker run -d exchange-survey-api
```

### Locally

Once a local MongoDB cluster is already running, run the following commands:

```bash
npm i
node src/service.js
```

## Requests

The requests are available as a Postman collection in the `resources` directory.
Import this collection into Postman for the full documentation and example requests for each
API endpoint.

The key endpoints are also listed below:

### POST a survey request

```bash
curl --location --request POST 'http://localhost:3000/survey' \
--header 'Content-Type: application/json' \
--data-raw '{
  "contact": {
    "email": "example@test.com",
    "role": "product-owner"
  },
  "responses": [
    {
      "question": "How would you rate the experience of locating assets on the Portal?",
      "response": "5"
    },
    {
      "question": "How would you rate the experience of completing the consume form?",
      "response": "2"
    },
    {
      "question": "How would you rate the quality of the Portal content?",
      "response": "1"
    },
    {
      "question": "How would you rate the quality of the Business and Technical documentation on the Portal?",
      "response": "4"
    }
  ],
  "feedback": "This is some test feedback from Postman"
}'
```

### GET all survey responses

```bash
curl --location --request GET 'http://localhost:3000/survey/all'
```

### GET all survey responses between specific dates

```bash
curl --location --request GET 'http://localhost:3000/survey/all?start=2022-10-13&end=2022-10-13'
```

### POST new password

```bash
curl --location --request POST 'http://localhost:3000/survey/password/new' \
--header 'Content-Type: application/json' \
--data-raw '{
    "password": "test-password"
}'
```

### Verify password against database

```bash
curl --location --request POST 'http://localhost:3000/survey/password/verify' \
--header 'Content-Type: application/json' \
--data-raw '{
    "password": "test-password"
}'
```

### GET number of responses

```bash
curl --location --request GET 'http://localhost:3000/survey/responses/count'
```

### GET user feedback per question (response counts)

```bash
curl --location --request GET 'http://localhost:3000/survey/responseCount'
```

### GET response counts by date

```bash
curl --location --request GET 'http://localhost:3000/survey/responseCount?start=2022-10-20&end=2022-10-20'
```
